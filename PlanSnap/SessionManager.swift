//
//  SessionManager.swift
//  PlanSnap
//
//  Created by Stageaire on 08/08/2018.
//  Copyright © 2018 Stageaire. All rights reserved.
//

import UIKit

class SessionManager: NSObject {
    
    static let shared : SessionManager = SessionManager()

    var userIsConnected: Bool {
        get{
            if(UserDefaults.standard.dictionary(forKey: "User") != nil){
                return true
            }
            return false
        }
    }
    
}
