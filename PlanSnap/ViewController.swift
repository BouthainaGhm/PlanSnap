//
//  ViewController.swift
//  PlanSnap
//
//  Created by Stageaire on 06/08/2018.
//  Copyright © 2018 Stageaire. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FBSDKLoginKit
import FBSDKCoreKit


class ViewController: UIViewController, FBSDKLoginButtonDelegate {
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(loginButton as UIView)
        loginButton.frame = CGRect(x: 95, y: 400, width:view.frame.width - 190, height:40)
        loginButton.delegate = self
        loginButton.readPermissions = ["email","public_profile"]
        loginButton.setTitle("Continue with Facebook", for: .normal)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
 
        let storyBoard = UIStoryboard (name: "Main", bundle: nil)
        let loginVC = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(loginVC, animated: true)

    }
    /*@IBAction func loginFBAction(_ sender: Any) {
        
            // User is logged in, do work such as go to next view controller.
        let loginManager = FBSDKLoginManager()
        let permissions = ["public_profile"]
        
        let handler = { (result: FBSDKLoginManagerLoginResult!, error: NSError?) in
            if let error = error {
                //3.1
                print("error = \(error.localizedDescription)")
            } else if result.isCancelled {
                //3.2
                print("user tapped on Cancel button")
            } else {
                //3.3
                print("authenticate successfully")
                let welcomeVC = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                self.navigationController?.pushViewController(welcomeVC, animated: true)
            }
        }
        //loginManager.logIn(withReadPermissions: permissions, from: self, handler: handler as! FBSDKLoginManagerRequestTokenHandler)
        
    }*/
    
    @IBAction func subscribeAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard (name: "Main", bundle: nil)
        let subscribeVC = storyBoard.instantiateViewController(withIdentifier: "SubscribingViewController") as! SubscribingViewController
        self.navigationController?.pushViewController(subscribeVC, animated: true)
    }
    
    let loginButton = FBSDKLoginButton()
     
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
     
    if error != nil {
     print(error)
    }
    else {
        if let error = error {
            //3.1
            print("error = \(error.localizedDescription)")
        } else if result.isCancelled {
            //3.2
            print("user tapped on Cancel button")
        } else {
            //3.3
            print("seccesfully logged in with facebook")
           //Facebook Access Token
            let accessToken = FBSDKAccessToken.current()
            guard let accessTokenString = accessToken?.tokenString else { return }
            print(accessTokenString)
            
            //Get credentials from FB
            let credential = FacebookAuthProvider.credential(withAccessToken: accessTokenString)
            
            //Sign in using those credentials
            Auth.auth().signInAndRetrieveData(with: credential, completion: { (user, err) in
                if err != nil {
                    print("error with firebase!", err!)
                }
                else {
                    print("success register in firebase")
                    let storyBoard = UIStoryboard (name: "Main", bundle: nil)
                    let welcomeVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                    self.navigationController?.pushViewController(welcomeVC, animated: true)
                }
            
            })
         }
        print("succes")
 
    }

}
    
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print ("did log out from facebook")
    }
    
    
}

