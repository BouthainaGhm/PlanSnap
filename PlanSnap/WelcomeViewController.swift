//
//  WelcomeViewController.swift
//  PlanSnap
//
//  Created by Stageaire on 06/08/2018.
//  Copyright © 2018 Stageaire. All rights reserved.
//

import UIKit
import FirebaseAuth
import FBSDKLoginKit
import FBSDKCoreKit

class WelcomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Actions
    @IBAction func logOutAction(_ sender: Any) {
        
        if FBSDKAccessToken.current() != nil {
            do {
                try Auth.auth().signOut()
                let viewController: UIViewController = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                let newNavigContr = UINavigationController(rootViewController: viewController)
                (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = newNavigContr
                print("logged out")
            } catch {
                print("error logging out")
            }
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
            
        } else{
            do {
                try Auth.auth().signOut()
                //FIXME: fix the error
                let viewController: UIViewController = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                let newNavigContr = UINavigationController(rootViewController: viewController)
                newNavigContr.isNavigationBarHidden = true
                (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = newNavigContr

                UserDefaults.standard.set(nil, forKey: "User")
                print("user default setted to nil")
                
                } catch {
                print("error logging out")
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
